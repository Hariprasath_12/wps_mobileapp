import { Injectable } from '@angular/core';

import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) { 

    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
    
  }

   /* End of budget planning module day data **/
   public get userValue(): User {
    return this.userSubject.value;
}

  getMyProjects(localData) {
    return this.http.post<any>(`${environment.apiUrl}/project/getallmyproject`, {})
    .pipe(map(responseData => {
        // console.log("THJis is Projects",responseData);
        return responseData;
    }));
}

fetchProjectById(projectId) {
  const user = this.userValue;
  return this.http.get<any>(`${environment.apiUrl}/project/projectbyid/`+ projectId)
  //return this.http.post<any>(`${environment.apiUrl}/document/getallprojectdocument`, {project: documentId})
  .pipe(map(responseData => {
      // console.log(responseData);
      return responseData;
  }));
}

fetchDocumentNames(projectId) {
  const user = this.userValue;
  return this.http.post<any>(`${environment.apiUrl}/document/getallprojectdocumentName`, {projectId: projectId})
  .pipe(map(responseData => {
      // console.log(responseData);
      return responseData;
  }));
}

  // SHOT DIVISION
  findShotDivision(project_id, document_id, userId, sluglineId, sceneId) {
    
    return this.http.get<any>(`${environment.apiUrl}/scenes/findShotDivision?projectId=` + project_id + `&documentId=` + document_id + `&userId=` + userId +
      `&sluglineId=` + sluglineId + `&sceneId=` + sceneId)
      .pipe(map(responseData => {
        console.log(responseData);
        return responseData;
      }));
  }


  //FETCH SCENES
  fetchScenes(projectId, documentId) {
    const user = this.userValue;
    return this.http.get<any>(`${environment.apiUrl}/scenes/fetchScenes`+`?projectId=` + projectId + `&documentId=` + documentId)
    .pipe(map(responseData => {
        // console.log(responseData);
        return responseData;
    }));
}
//FETCH ONE SCENE DETAILS
FetchSceneInfobySceneId(projectId, documentId, sceneId) {
      
  return this.http.get<any>(`${environment.apiUrl}/scenes/findScenesInfoBySceneId?projectId=` + projectId + `&documentId=` + documentId + `&scene_id=` + sceneId)
  .pipe(map(responseData => {
      console.log(responseData);
      return responseData;
  }));
}

}
