export class User {
    id: string;
    email: string;
    password?: string; // interfaces allow fields to be optional
    confirm_password?: string; // interfaces allow fields to be optional
    mobile?: string;
    first_name: string;
    last_name: string;
    token: string;
    account_type: string;
}