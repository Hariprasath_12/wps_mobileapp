import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { Form, FormBuilder,FormControl,FormGroup,Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { ProjectService } from '../../../services/project.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.page.html',
  styleUrls: ['./documents.page.scss'],
})
export class DocumentsPage implements OnInit {
  // documentid="6194ef41a1a57d0dac386030";
  documentList : any = [];
  projectName:any='mani';
  user_id: string = "";
  project_id: string = "";
  project_name: string = "";
  moduleInnerLoading: boolean = false;
  isdocumentsFound: boolean = false;
  projectId: string = null;
  documentId: string = null;
  modalReference : any;
  // categoryFormControl: FormGroup;
  // documentNameControl = new FormControl('', [Validators.required]);
  page : any;
  isButtonVisible = false;
  NewCat : string = "newcategory";

  constructor(

    private formBuilder: FormBuilder, 
  //  private modalService: NgbModal, 
  //  private datePipe: DatePipe,
    private projectService: ProjectService, 
    private authService: AuthService, 
    // private toastr: ToastrService, 
    private router: Router,
    // private navigationService: NavigationService,
    private Activatedroute:ActivatedRoute, 
    private CommonService: CommonService
  ) { }

  @ViewChild('template') template: ElementRef;
  
  ngOnInit() {
    localStorage.removeItem('documentId');
    this.projectName=localStorage.getItem('projectName');
    console.log("project name",this.projectName);

    this.Activatedroute.paramMap.subscribe(params => { 
      this.project_id = params.get('project_id'); 
      this.page = params.get('data'); 
      this.fetchProjectDocuments();
    });

    console.log("Printing Current Project Name: ", this.CommonService.currentProjectName );
  }

  closeResult = '';

  fetchProjectDocuments() {
    this.moduleInnerLoading = true;

   this.project_id= localStorage.getItem('projectId');

    let projectId = this.project_id ;

    // console.log("THis is ",projectId);
    this.projectService.fetchDocumentNames(projectId)
    .subscribe(
      data => {
        // console.log(data);
        
        if(data.success == true) {
          console.log("this is hari reference ",data);
          if (data.response.length != 0) {
            this.documentList = data.response;
            this.isdocumentsFound=true;

            console.log("Thid is Documrnts",this.documentList);
            // $('.disp').show();
            // $('.imgdisp').hide();

            // this.documentList.forEach((docs, index) => {
            //   if(docs.lockstatus) {
            //     if(docs.lockstatus == "locked") {
            //       docs.checked = true;
            //     } else {
            //       docs.checked = false;
            //     }
            //   } else {
            //     docs.checked = false;
            //   }
            // });

          }
          // else{
          //   this.documentList = data.response;
          //   // $('.disp').hide();
          //   // $('.imgdisp').show();
            
          //   // SHOW CREATE DOCUMENT POPUP ON-LOAD IF NO DOCUMENT IS AVAILABLE
          //   // this.modalReference = this.modalService.open(this.template, {ariaLabelledBy: 'modal-basic-title'});
          //   this.modalReference.result.then((result) => {
          //     this.closeResult = `Closed with: ${result}`;
          //   }, (reason) => {
          //     // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          //   });
            
          // }
          
        }
        // this.moduleInnerLoading = false;
        // console.log(this.documentList);
      }
    );

  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }


  // logout
  logout() {
    this.authService.logout();
    // console.log("in my room work",this.userDetails);
   
}
shotlist(documentId,documentName){
  console.log('Dog id',documentId);
  console.log("document name",documentName);
  // this.router.navigate(['projects','/shotlist']);
  localStorage.setItem('documentId',documentId);
  // localStorage.setItem('documentName',documentName);
  // let name=localStorage.getItem('documentName');
  // console.log("this local storage name",name);
  this.router.navigate(['projects/shotlist']);
  // localStorage.setItem('documentid',documentid);
}
backicon(){
  this.router.navigate(['projects']);
}

}
