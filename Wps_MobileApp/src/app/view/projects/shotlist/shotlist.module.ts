import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShotlistPageRoutingModule } from './shotlist-routing.module';

import { ShotlistPage } from './shotlist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShotlistPageRoutingModule
  ],
  declarations: [ShotlistPage]
})
export class ShotlistPageModule {}
