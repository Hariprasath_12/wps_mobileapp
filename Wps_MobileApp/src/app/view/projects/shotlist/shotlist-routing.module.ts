import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShotlistPage } from './shotlist.page';

const routes: Routes = [
  {
    path: '',
    component: ShotlistPage
  },
  {
    path: 'tabs',
    loadChildren: () => import('./shotlist-tab/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'shotlisdetails',
    loadChildren: () => import('./shotlisdetails/shotlisdetails.module').then( m => m.ShotlisdetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShotlistPageRoutingModule {}
