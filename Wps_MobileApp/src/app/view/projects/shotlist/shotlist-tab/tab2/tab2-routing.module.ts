import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Tab2Page } from './tab2.page';
import { TakepopupComponent } from './takepopup/takepopup.component';

const routes: Routes = [
  {
    path: '',
    component: Tab2Page
  },
  {
    path: 'take-popup',
    component: TakepopupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Tab2PageRoutingModule {}
