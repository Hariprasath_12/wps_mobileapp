import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShotlisdetailsPage } from './shotlisdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ShotlisdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShotlisdetailsPageRoutingModule {}
