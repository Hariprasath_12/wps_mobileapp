import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShotlisdetailsPageRoutingModule } from './shotlisdetails-routing.module';

import { ShotlisdetailsPage } from './shotlisdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShotlisdetailsPageRoutingModule
  ],
  declarations: [ShotlisdetailsPage]
})
export class ShotlisdetailsPageModule {}
