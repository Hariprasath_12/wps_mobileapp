import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-shotlisdetails',
  templateUrl: './shotlisdetails.page.html',
  styleUrls: ['./shotlisdetails.page.scss'],
})
export class ShotlisdetailsPage implements OnInit {
  Data=[1,2,3,4,5,6,7,8,9,10];
  shotLists: any = [];
  shotList: any = [];
  userDetails: any = [];
  documentId:any='';
  userId:any='';
  userwholedetails:any='';
  projectId:any='';
  sceneId:any='';
  sceneData:any=[];
  sluglineId:any='';
  constructor(
    private router:Router,
    private projectService: ProjectService,
    private commonservice:CommonService
  ) { }

  ngOnInit() {
    
    this.userDetails=localStorage.getItem('user');
    this.userId=JSON.parse(this.userDetails);
    // this.userId=this.userDetails._id;
    // console.log("this is user Id",this.userId);
   this.documentId=localStorage.getItem('documentId');
   this.projectId=localStorage.getItem('projectId');
   this.sceneId=localStorage.getItem('sceneId');
   this.sluglineId=localStorage.getItem('sluglineId');
   console.log("projectID",this.projectId);
   console.log("document id",this.documentId);
   console.log("sceneId",this.sceneId);
   console.log("slugline Id",this.sluglineId);
   this.getSluglineShots();
  //  this.FetchSceneInfobySceneId();
  //  console.log("this is document id",this.documentId,"tis is project id",this.projectId,"this is scene id",this.sceneId); 
  }

  getSluglineShots() {
    // const user = this.authService.userValue;
    // const projectId = '6209ea1b565bd5589411c472';
    // const documentId = '620b04f714988b10b492ade6';
    // const userId = '603cde7c64100e0a2c3b704f';
    // const sluglineId = 'SL_0_047387f3-018c-4696-9e1e-857debd39e6e';
    // const sceneId = 'ST_f06eba01-9017-4488-b13b-d2190d16a909';
    // console.log("welco");
    // return false;
    // this.ShotDivisonService.findShotDivision(this.project_id, this.document_id, user.id, this.sluglineId, this.sceneId)
    this.projectService.findShotDivision(this.projectId, this.documentId, this.userId.id, this.sluglineId, this.sceneId)
      .subscribe(
        data => {
          console.log("datas",data)
          this.shotList = data.shotDivisionList[0].shotList;
          // this.shotLists=this.shotLists[0];
          console.log("Printing Shot list: ",this.shotList);
          
        });
  }

  FetchSceneInfobySceneId(){
    this.projectService.FetchSceneInfobySceneId(this.projectId,this.documentId,this.sceneId)
    .subscribe(
      data => {
        console.log("from sinthu",data);
        // this.sceneData=data.scenesList[0].scenesList;
        // console.log("this is response data",this.sceneData);
        
      }
    )
  }

  ShowDetails(shotType){
    this.commonservice.shot_type=shotType;
    console.log("this is shot type 123",shotType);
    // localStorage.setItem('shot_type',shottype);
    this.router.navigateByUrl('projects/shotlist/tabs/tab1');
  }
  backicon(){
    this.router.navigateByUrl('projects/shotlist');
  }
  showtake(){
    this.router.navigateByUrl('projects/shotlist/tabs/tab2');
    // this.router.navigate(['projects/shotlist/tabs'])
  }
  // backicon(){

  // }
}
